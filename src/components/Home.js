import BlogItems from "./BlogItems";
import React from 'react';
import './Home.css'


const Home = () => {
  const blogList = [
    {
      id: 1,
      title: "My first post",
      description: "A high quality solution beautifully designed for startups",
      publishedDate: "Aug 2nd",
    },
    {
      id: 2,
      title: "My second post",
      description: "A high quality solution beautifully designed for startups",
      publishedDate: "Mar 1st",
    },
    {
      id: 3,
      title: "My third post",
      description: "A high quality solution beautifully designed for startups",
      publishedDate: "Jan 2nd",
    },
    {
      id: 4,
      title: "My fourth post",
      description: "A high quality solution beautifully designed for startups",
      publishedDate: "Dec 23rd",
    },
    {
      id: 5,
      title: "My fifth post",
      description: "A high quality solution beautifully designed for startups",
      publishedDate: "Nov 10th",
    },
  ];

  return (
    <div className="homeContainer">
      <div className="profileInfo">
        <img  className="imageUrl" src="https://assets.ccbp.in/frontend/react-js/profile-img.png" />
        <br />
        <p className="nameText">Wade Warner</p>
        <p className="profileText">Software Devloper Of Uk</p>
      </div>
        {blogList.map((eachItem)=>{
        return <BlogItems eachItem={eachItem}/>
        })}
      
    </div>
  );
};
export default Home;
