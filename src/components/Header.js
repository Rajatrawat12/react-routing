import React from 'react';
import Home from './Home';
import About from './About';
import "./Header.css"
import { Link } from "react-router-dom";
const Header = () => (
    <div className='headerContainer'>
    <div>
    <img  className="logoUrl" src="https://1000logos.net/wp-content/uploads/2016/10/Amazon-logo-meaning.jpg"/>
    </div>
     <ul className="nav-menu">
       <li>
         <Link className="nav-link" to="/">Home</Link>
       </li>
       <li>
         <Link className="nav-link" to="/about">About</Link>
       </li>
       <li>
         <Link className="nav-link" to="/contact">Contact</Link>
       </li>
     </ul>
     </div>

)
export default Header