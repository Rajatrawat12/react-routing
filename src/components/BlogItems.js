
import React from 'react';
import "./BlogItems.css"
const BlogItems=(props)=>{
const  {eachItem}= props
const{id,title,description,publishedDate}=eachItem

return(
   <div className= "blogListContainer">
   <div>
   <p className='tiltleText'>{title}</p>
   <p className='descriptionText'>{description}</p>
   </div>
   <div>
   <p className='dateText'>{publishedDate}</p>
   </div>
   </div> 
)

}
export default BlogItems