import Header from './components/Header';
import Home from './components/Home';
import Contact from './components/Contact';
import About from './components/About';
import NotFound from './components/NotFound';
import React from 'react';
import { BrowserRouter, Route, Routes } from "react-router-dom";
import './App.css';
const App =() =>
   (
  <BrowserRouter>
  <Header />
    <Routes>
      <Route exact path="/" element={<Home/>} />
      <Route exact path="/about" element={<About/>} />
      <Route exact path="/contact" element={<Contact/>} />
      <Route exact path="/*" element={<NotFound />} />
    </Routes>
</BrowserRouter>
  )


export default App;
